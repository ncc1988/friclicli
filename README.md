# About friclicli

Friclicli is a CLI client for the decentralised social network Friendica.
The name friclicli is an abbreviation for Friendica CLI client.

# Compilation and installation

To compile friclicli, install the following dependencies first using the
package management of your operating system.

- CMake:
  Friclicli uses CMake as build system.

- libcurl:
  NOTE: Depending on your operating system you may choose between different
  flavours of libcurl which are linked to different SSL libraries.
  The choice of SSL library is (probably) irrelevant for friclicli.

- libncursesw and libncurses:
  Friclicli needs those libraries for its GUI.

After that, you have to fetch the cJSON library as additional dependency.
It is included as git submodule in the friclicli repository. All you have to do
is run the following git commands in the main directory of the friclicli
repository:

    git submodule init
    git submodule update


After the installation of the dependencies, create a new folder called "build"
inside the main directory of the friclicli repository and execute cmake
from a shell inside the "build" directory:

    cmake ..

This will tell cmake to build everything inside the "build" directory
without mixing build files with the rest of the repository.

If you want to debug friclicli you should run cmake from the "build" directory
with the parameter CMAKE_BUILD_TYPE=Debug set:

    cmake -DCMAKE_BUILD_TYPE=Debug ..

When cmake has finished you can simply run "make" for single core computers
or "make -jX" for multi core computers where X is the number of CPU cores your
computer has.

To install friclicli system-wide (not recommended at this moment) you can
just run "make install" in the "build" directory as root.


# Randomly answered questions

(There are no questions about friclicli yet so naming this section FAQ
would be wrong.)

## Why write a CLI client for a social network?

There are several reasons to do so:

### Being compatible with old hardware

There are computers out there that don't work well with the Friendica GUI.
On old computers from the last decade your Friendica instance may load for
half a minute in your browser. But also modern computers like the Rasperry Pi
will require a lot of time (minutes) until the Friendica page is loaded
in Firefox. I know from experience ;)

CLI software has the advantage of being fast even on old hardware with poor
graphics performance. You may want to try the Mutt/NeoMutt E-Mail client
on the Raspberry Pi to know what I mean ;)

Having a CLI client for Friendica would mean being able to use Friendica
on a Raspberry Pi computer or a similar computer in text mode.
Furthermore one could probably use friclicli on old computers from the 90s
that are still supported by free operating systems
and use those computers longer.

If all software would be written so that it can run on
old hardware we would have less electronic waste since people can use
their computers a lot longer.

### There are people out there who like CLI software

I (and other people out there ) like an interface that is cleaned up
and easy to use from the keyboard.
If you know the Mutt/NeoMutt E-Mail client you know how convenient
it can be to manage your mails using nothing but your keyboard.
The same can be done for Friendica.

## Does friclicli have a Code of Conduct?

No, and I don't plan to add a Code of Conduct in the style
the Linux kernel adopted it recently (around 2018-09).
For me, everything is fine as long as people contribute to this project
in the form of source code, bug reports or by
criticising this project in a constructive manner.



----------------


You have reached the end of the README.md file.
I wish you a pleasant day.


Regards,
Moritz
