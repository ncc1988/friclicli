# Friendica GUI to API

This document explains which GUI action is represented by which API route.

## Timelines

Your profile (a.k.a. the "home button"):

    statuses/user_timeline?exclude_replies=true

The "stream" (posts from you and your contacts):

    statuses/home_timeline?exclude_replies=true

## Messages

Your messages (the message button):

    direct_messages/all
