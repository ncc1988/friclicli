#!/bin/bash


# To be able to use this script you must have the programs
# curl and json_pp installed. Furthermore you need to add a configuration
# file named "friendica-config.sh". This file is included here and must define
# the following three variables:
# friendica_url: The URL to a friendica instance
#     where you have an account you want to use
#     with this script. The URL must end with a
#     trailing slash character.
# user: The Friendica username.
# password: The password for the user.
#
# Example friendica-config.sh file:
#
# friendica_url='https://f.example.org/'
# user='test'
# password='test'
#


if [ $# -lt 2 ]
then
    echo "Usage: friendica-api-request.sh METHOD API_ROUTE [PARAMETERS]"
    echo "METHOD:\tThe HTTP method to use."
    echo "API_ROUTE:\tThe API route which shall be queried."
    echo "PARAMETERS:\tOptional URL parameters in the form key=value."
    exit 0
fi


source ./friendica-config.sh
if [ $? -ne 0 ]
then
    echo "Error including friendica-config.sh file."
    echo "Please make sure that the file exists and that it is not empty."
    echo "See the header comment of this script for instructions on"
    echo "how to create the friendica-config.sh file."
    exit 1
fi

#Rerieve data from arguments:
method=$1
api_route=$2

# $url_parameters contains all parameters including
# the -d option for curl.
url_parameters=''
# Before building the url_parameters string we must skip the first
# two script arguments and disable space characters as delimiter
# for arguments:
shift 2
IFS='
'
for i in $@
do
    url_parameters="$url_parameters --data-urlencode \"$i\""
done

url="$friendica_url/api/$api_route"

result=$(curl -s -X $method -u "$user:$password" "$url" $url_parameters)
echo $result | json_pp -f json -t json -json_opt allow_nonref,pretty
if [ $? -ne 0 ]
then
    echo "Raw result data:"
    echo $result
    echo
fi

#This script has finished.
