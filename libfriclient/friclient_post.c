/*
    This file is part of libfriclient.
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "friclient_post.h"


//Functions for freeing FriclientPost structures:


FriclientPost* friclient_read_post_from_json(cJSON* json)
{
    if (json == NULL) {
        //Nothing to do.
        return NULL;
    }

    //TODO: more checks of the JSON data structure.

    FriclientPost* post = malloc(sizeof(FriclientPost));
    if (post == NULL) {
        return NULL;
    }
    memset(post, 0, sizeof(FriclientPost));

    cJSON* post_id_attr = cJSON_GetObjectItemCaseSensitive(json, "id");
    if (post_id_attr != NULL) {
        post->post_id = post_id_attr->valueint;
    }
    cJSON* user_attr = cJSON_GetObjectItemCaseSensitive(json, "user");
    if (user_attr != NULL) {
        FriclientUser* user = friclient_read_user_from_json(user_attr);
        if (user != NULL) {
            post->user = user;
        }
    }
    cJSON* text_attr = cJSON_GetObjectItemCaseSensitive(json, "text");
    if (text_attr != NULL) {
        friclient_post_set_text(
            post,
            text_attr->valuestring
            );
    }

    cJSON* html_attr = cJSON_GetObjectItemCaseSensitive(json, "html");
    if (html_attr != NULL) {
        friclient_post_set_html_text(
            post,
            html_attr->valuestring
            );
    }

    return post;
}


void friclient_free_post_reaction(FriclientPostReaction* reaction)
{
    if (reaction == NULL) {
        //Don't worry. It's just the pointer which is null.
        //It doesn't necessary mean that your post created
        //no reactions. ;)
        return;
    }

    if (reaction->verb != NULL) {
        size_t verb_length = strlen(reaction->verb);
        memset(reaction->verb, 0, verb_length);
        free(reaction->verb);
    }

    memset(reaction, 0, sizeof(FriclientPostReaction));
    free(reaction);
}


void friclient_free_post(FriclientPost* post)
{
    if (post == NULL) {
        return;
    }

    if (post->text != NULL) {
        memset(post->text, 0, post->text_size);
        free(post->text);
    }

    if (post->html_text != NULL) {
        memset(post->html_text, 0, post->html_size);
        free(post->html_text);
    }

    if (post->reactions != NULL) {
        size_t i = 0;
        for (i = 0; i < post->reactions_size; i++) {
            if (post->reactions[i] != NULL) {
                friclient_free_post_reaction(post->reactions[i]);
            }
        }
        memset(post->reactions, 0, post->reactions_size);
        free(post->reactions);
    }

    memset(post, 0, sizeof(FriclientPost));
    free(post);
}


void friclient_free_post_collection(FriclientPostCollection* collection)
{
    if (collection == NULL) {
        return;
    }

    if (collection->posts != NULL) {
        size_t i = 0;
        for (i = 0; i < collection->size; i++) {
            if (collection->posts[i] != NULL) {
                friclient_free_post(collection->posts[i]);
            }
        }
        memset(collection->posts, 0, collection->size);
        free(collection->posts);
    }

    memset(collection, 0, sizeof(FriclientPostCollection));
    free(collection);
}


//FriclientPost service functions:


uint16_t friclient_post_count_likes(FriclientPost* post)
{
    return 0;
}


uint16_t friclient_post_count_dislikes(FriclientPost* post)
{
    return 0;
}


uint16_t friclient_post_count_reshares(FriclientPost* post)
{
    if (post == NULL) {
        return 0;
    }
    return (uint16_t)post->reshares_size;
}


uint16_t friclient_post_count_comments(FriclientPost* post)
{
    if (post == NULL) {
        return 0;
    }
    return (uint16_t)post->replies_size;
}


void friclient_post_set_text(FriclientPost* post, const char* text)
{
    if ((post == NULL) || (text == NULL)) {
        //Nothing to do.
        return;
    }

    if (post->text) {
        //Free the memory for the old text first:
        memset(post->text, 0, post->text_size);
        free(post->text);
    }

    size_t new_text_size = strlen(text) + 1;

    if (new_text_size > 1) {
        //There is more to allocate than only the null charater.
        post->text = malloc(new_text_size);
        memset(post->text, 0, new_text_size);
        strcpy(post->text, text);
    }
}


void friclient_post_set_html_text(FriclientPost* post, const char* html_text)
{
    if ((post == NULL) || (html_text == NULL)) {
        //Nothing to do.
        return;
    }

    if (post->html_text) {
        //Free the memory for the old HTML text first:
        memset(post->html_text, 0, post->html_size);
        free(post->html_text);
    }

    size_t new_html_size = strlen(html_text) + 1;

    if (new_html_size > 1) {
        //There is more to allocate than only the null charater.
        post->html_text = malloc(new_html_size);
        memset(post->html_text, 0, new_html_size);
        strcpy(post->html_text, html_text);
    }
}
