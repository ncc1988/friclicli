/*
    This file is part of libfriclient.
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FRICLIENT_USER__H
#define FRICLIENT_USER__H


#include <malloc.h>
#include <inttypes.h>
#include <string.h>


#include "cJSON/cJSON.h"


struct FriclientUser {
    uint64_t user_id;
    uint64_t contact_id;
    char* name;
    size_t name_size;
    char* screen_name;
    size_t screen_name_size;
    char* location;
    size_t location_size;
};
typedef struct FriclientUser FriclientUser;


FriclientUser* friclient_read_user_from_json(cJSON* json_data);


/**
 * Returns either the name or the user-ID from a FriclientUser type.
 * If the name is set it is returned. In case no name is set
 * the user-ID will be returned with "UID " prepended to it.
 *
 * @param FriclientUser* user The user object whose name shall be returned.
 *
 * @returns char* The user name. If that isn't there: the User-ID.
 *     In case of an error, NULL is returned.
 */
char* friclient_user_get_name_or_id(FriclientUser* user);


#endif
