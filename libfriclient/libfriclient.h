/*
    friclicli
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIBFRICLIENT__H
#define LIBFRICLIENT__H

#include <malloc.h>
#include <string.h>


#include <curl/curl.h>
#include "cJSON/cJSON.h"


#include "friclient_handle.h"
#include "friclient_user.h"
#include "friclient_post.h"


typedef struct {
    char* data;
    size_t size;
    size_t allocated_size;
}FriclientData;


/**
 * This enum defines HTTP methods.
 */
enum {
    FRICLIENT_HTTP_GET,
    FRICLIENT_HTTP_POST,
    FRICLIENT_HTTP_PUT,
    FRICLIENT_HTTP_DELETE
};


#define FRICLIENT_RESPONSE_READ_BLOCK_SIZE 32768 //Read response data in 32 KiB blocks


//Internal functions:


size_t friclient_read_response(void* buffer, size_t size, size_t nmemb, FriclientData* response);


//TODO: general friendica_request function that lets the program
//select between the JSON and XML result format.


/**
 * This function performs an HTTP request and returns a cJSON object
 * that can be processed further in the calling function.
 */
cJSON* friclient_request_json(
    FriclientHandle* handle,
    uint8_t http_method,
    char* api_route,
    char** url_parameters,
    size_t url_parameter_count,
    FriclientData* body
    );


/**
 * Sets the authentication credentials to the CURL structure
 * inside the handle.
 */
void friclient_set_auth_credentials(FriclientHandle* handle);


//Debug functions:


/**
 * Dumps the received data into a file.
 *
 * @returns 0 if everything went fine, 1 in case of error.
 */
uint8_t friclient_dump_json(char* filename, cJSON* json);


//Initialisation and cleanup functions:


/**
 * Creates a FriclientHandle which is needed to perform
 * requests.
 * NOTE: curl_global_init() is not called here since it might
 * interfer with code from programs using this library.
 * The global curl initialisation must therefore always be done in the
 * program that uses this library!
 *
 * @param const char* handle_string A string containing the username
 *     and the domain of the Friendica user. Example: test@example.org
 *
 * @returns FriclientHandle* A FriclientHandle structure or NULL in case
 *     of failure.
 */
FriclientHandle* friclient_init(
    const char* handle_string
    );


//Functions for the Friendica API:


/**
 * Retrieves the server information and puts it inside
 * the FriclientHandle instance.
 *
 * @returns 0 if everything went fine, 1 in case of error.
 */
uint8_t friclient_get_server_info(FriclientHandle* handle);


/**
 * @returns 1 in case of error, 0 on successful login.
 */
uint8_t friclient_plain_login(
    FriclientHandle* handle
    );


uint8_t friclient_oauth1_login(
    FriclientHandle* handle,
    const char* token,
    const char* secret
    );


void friclient_logout(FriclientHandle* handle);


void friclient_read_notifications(FriclientHandle* handle);


/**
 * Reads the timeline of the currently logged in user.
 *
 * @param FriclientHandle* handle The friclient handle.
 * @param uint8_t include_private_posts Whether private posts
 *     (posts to a limited number of contacts)
 *     shall be included (1) or not (0). Defaults to 0.
 */
FriclientPostCollection* friclient_read_timeline(
    FriclientHandle* handle,
    uint8_t include_private_posts
    );


/**
 * Reads the posts of the user's community.
 *
 * @param FriclientHandle* handle The friclient handle.
 */
FriclientPostCollection* friclient_read_community_posts(
    FriclientHandle* handle
    );


/**
 * Reads one specific post.
 *
 * @param const chra* post_id The ID of the post that shall be read.
 *
 * @param uint8_t with_conversations If this is set to a value greater than zero
 *     the post is retrieved including the conversations attached to it.
 *     If the value of this parameter is zero, only the post is returned.
 *
 * @returns FriclientPost* In case a post could be retrieved a valid pointer
 *     to a FriclientPost object is returned. If no post could be read,
 *     that pointer is NULL.
 */
FriclientPost* friclient_read_post(
    FriclientHandle* handle,
    const char* post_id,
    uint8_t with_conversations
    );


FriclientPost* friclient_send_post(
    FriclientHandle* handle,
    const char* title,
    const char* text,
    uint64_t* recipient_user_ids
    );


void friclient_delete_post(FriclientHandle* handle, char* post_id);


/**
 * @returns char* The content of the comment.
 */
char* friclient_read_comment(FriclientHandle* handle, char* comment_id);


/**
 * @returns char* The ID of the new comment.
 */
char* friclient_send_comment(FriclientHandle* handle, char* post_id, char* text);


char* friclient_read_message(
    FriclientHandle* handle,
    char* message_id
    );


void friclient_send_message(
    FriclientHandle* handle,
    char** recipient_user_ids,
    char* topic,
    char* text
    );


char* friclient_delete_message(
    FriclientHandle* handle,
    char* message_id
    );


uint16_t friclient_count_likes(FriclientPost* post);


uint16_t friclient_count_dislikes(FriclientPost* post);


uint16_t friclient_count_reshares(FriclientPost* post);


uint16_t friclient_count_comments(FriclientPost* post);


#endif
