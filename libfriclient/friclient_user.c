/*
    This file is part of libfriclient.
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "friclient_user.h"


FriclientUser* friclient_read_user_from_json(cJSON* json)
{
    if (json == NULL) {
        //Nothing to do.
        return NULL;
    }

    if (!cJSON_IsObject(json)) {
        //Invalid data.
        return NULL;
    }

    FriclientUser* user = malloc(sizeof(FriclientUser));
    if (user == NULL) {
        return NULL;
    }
    memset(user, 0, sizeof(FriclientUser));

    //Read the user-ID:
    cJSON* user_id_attr = cJSON_GetObjectItemCaseSensitive(json, "id");
    if (user_id_attr != NULL) {
        user->user_id = user_id_attr->valueint;
    }

    //Read the contact-ID:
    cJSON* contact_id_attr = cJSON_GetObjectItemCaseSensitive(json, "cid");
    if (contact_id_attr != NULL) {
        user->contact_id = contact_id_attr->valueint;
    }

    //Read the user's name and screen name:
    cJSON* name_attr = cJSON_GetObjectItemCaseSensitive(json, "name");
    if (name_attr != NULL) {
        size_t name_size = strlen(name_attr->valuestring) + 1;
        user->name = malloc(name_size);
        user->name_size = name_size;
        memset(user->name, 0, name_size);
        strcpy(user->name, name_attr->valuestring);
    }

    cJSON* screen_name_attr = cJSON_GetObjectItemCaseSensitive(
        json,
        "screen_name"
        );
    if (screen_name_attr != NULL) {
        size_t screen_name_size = strlen(screen_name_attr->valuestring) + 1;
        user->screen_name = malloc(screen_name_size);
        user->screen_name_size = screen_name_size;
        memset(user->screen_name, 0, screen_name_size);
        strcpy(user->screen_name, screen_name_attr->valuestring);
    }

    //Now the location:
    cJSON* location_attr = cJSON_GetObjectItemCaseSensitive(json, "location");
    if (location_attr != NULL) {
        size_t location_size = strlen(location_attr->valuestring) + 1;
        user->location = malloc(location_size);
        user->location_size = location_size;
        memset(user->location, 0, location_size);
        strcpy(user->location, location_attr->valuestring);
    }

    return user;
}


char* friclient_user_get_name_or_id(FriclientUser* user)
{
    if (user == NULL) {
        return NULL;
    }

    if (user->name != NULL) {
        char* name = malloc(user->name_size);
        memset(name, 0, user->name_size);
        strcpy(name, user->name);
        return name;
    }
    //Name is not set. Return the user-ID instead and prepend "UID ".
    //An uint64_t value can be 20 characters long. Therfore we must
    //reserve 25 bytes.
    char* uid = malloc(25);
    if (uid == NULL) {
        return NULL;
    }
    sprintf(
        uid,
        "UID %ull",
        user->user_id
        );
    return uid;
}
