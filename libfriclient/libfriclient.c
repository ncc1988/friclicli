/*
    friclicli
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "libfriclient.h"


//Internal functions:


size_t friclient_read_response(
    void* buffer,
    size_t size,
    size_t nmemb,
    FriclientData* response
    )
{
    if (response == NULL) {
        //Allocate memory:
        response = malloc(sizeof(FriclientData));
        if (response == NULL) {
            //Error while allocating:
            return 0;
        }

        response->allocated_size = FRICLIENT_RESPONSE_READ_BLOCK_SIZE;
        response->size = response->allocated_size;
        response->data = malloc(response->size);
    }

    size_t new_size = size * nmemb + response->size;
    if (new_size > response->allocated_size) {
        //Allocate at least one more block:
        size_t new_block_count = new_size / FRICLIENT_RESPONSE_READ_BLOCK_SIZE;
        if ((new_size % FRICLIENT_RESPONSE_READ_BLOCK_SIZE) > 0) {
            //Add another block:
            new_block_count++;
        }

        //Prepare for appending buffer to response->data:
        char* tmp_data = NULL;
        if (response->size > 0) {
            tmp_data = malloc(response->size);
            memcpy(tmp_data, response->data, response->size);
            free(response->data);
        }

        response->data = malloc(
            response->allocated_size + (
                FRICLIENT_RESPONSE_READ_BLOCK_SIZE * new_block_count
                )
            );
        if (response->data == NULL) {
            //Somethin went wrong:
            free(tmp_data);
            return 0;
        }

        //Now do the append:
        if (tmp_data != NULL) {
            memcpy(response->data, tmp_data, response->size);
            free(tmp_data);
        }
    }
    //Append the new received data:
    memcpy(response->data + response->size, buffer, size * nmemb);
    //Update response->size:
    response->size = new_size;

    //Return the amount of what we have read:
    return size * nmemb;
}


cJSON* friclient_request_json(
    FriclientHandle* handle,
    uint8_t http_method,
    char* api_route,
    char** url_parameters,
    size_t url_parameter_count,
    FriclientData* body
    )
{
    if ((handle == NULL) || (api_route == NULL)) {
        //What shall we do without a handle or without
        //an API route?
        return NULL;
    }

    //Check if the http_method is valid:
    if (http_method > FRICLIENT_HTTP_DELETE) {
        //Undefined method.
        return NULL;
    }

    //We must sanitise the api_route parameter:
    //Character sequences like "../" must be removed.
    char* sanitised_route = malloc(strlen(api_route) + 1);
    if (sanitised_route == NULL) {
        //Error during memory allocation:
        return NULL;
    }
    memset(sanitised_route, 0, strlen(api_route) + 1);

    //Now we can replace all occurences of "../":
    /*
    //The following code is not working properly!
    size_t sanitised_offset = 0;
    size_t last_offset = 0;
    char* bad_sequence_ptr = strstr(api_route, "../");
    while (bad_sequence_ptr != NULL) {
        size_t current_offset = bad_sequence_ptr - api_route;
        size_t current_length = current_offset - last_offset;
        strncpy(
            sanitised_route + sanitised_offset,
            api_route + last_offset,
            current_length
            );
        //Update the offsets:
        sanitised_offset += current_length;
        last_offset = current_offset + 3;
        //Search for the next bad sequence:
        bad_sequence_ptr = strstr(api_route + last_offset, "../");
    }
    sanitised_route[sanitised_offset] = '\0';*/
    //Temporary workaround for not working code:
    strcpy(sanitised_route, api_route);
    //At this point we should have a sanitised api route:
    //#ifdef DEBUG
    //fprintf(stderr, "DEBUG: sanitised_route: [%s]\n", sanitised_route);
    //#endif

    //Now we build a complete url. But first we have to find out
    //how long it is:
    size_t base_url_length = strlen(handle->friendica_base_url);
    size_t url_length = base_url_length + strlen(sanitised_route);
    uint8_t add_slash = 0;
    if (sanitised_route[0] != '/' && handle->friendica_base_url[base_url_length - 1] != '/') {
        //No slash is found at the beginning of the sanitised api route
        //or the end of the friendica base url.
        //We must add that slash later and reserve memory for it.
        url_length++;
        add_slash = 1;
    }
    //we must add one byte for the string to be null terminated:
    url_length++;

    char* url = malloc(url_length);
    if (url == NULL) {
        //Error during memory allocation:
        return NULL;
    }
    memset(url, 0, url_length);

    strcat(url, handle->friendica_base_url);
    if (add_slash == 1) {
        //Since we initialised url with zeros the string is automatically
        //null-terminated by one of the remaining bytes.
        url[strlen(url)] = '/';
    }
    strcat(url, sanitised_route);
    //Our request-URL is finished.
    //sanitised_route is not needed anymore:
    free(sanitised_route);
    //#ifdef DEBUG
    //fprintf(stderr, "DEBUG: request-URL: [%s]\n", url);
    //#endif

    //Set the HTTP method as specified:
    switch (http_method) {
        case FRICLIENT_HTTP_POST: {
            curl_easy_setopt(handle->curl_handle, CURLOPT_POST, 1);
            if (body != NULL) {
                if (body->data != NULL) {
                    //Set the post fields from data:
                    curl_easy_setopt(
                        handle->curl_handle,
                        CURLOPT_POSTFIELDS,
                        body->data
                        );
                }
            }
            break;
        }
        default: {
            //HTTP GET is the default:
            curl_easy_setopt(handle->curl_handle, CURLOPT_HTTPGET, 1);
        }
    }

    //Now we do the request:
    curl_easy_setopt(handle->curl_handle, CURLOPT_URL, url);
    FriclientData* response = malloc(sizeof(FriclientData));
    if (response == NULL) {
        //Error during memory allocation:
        return NULL;
    }
    memset(response, 0, sizeof(FriclientData));
    response->data = NULL;
    curl_easy_setopt(handle->curl_handle, CURLOPT_WRITEDATA, response);

    CURLcode status_code = curl_easy_perform(handle->curl_handle);

    #ifdef DEBUG
    if (status_code != CURLE_OK) {
        fprintf(
            stderr,
            "DEBUG: Error calling url %s: %s\n",
            url,
            curl_easy_strerror(status_code)
            );
    }
    #endif

    //Now we read the JSON from the response:
    return cJSON_Parse(response->data);
}


void friclient_set_auth_credentials(FriclientHandle* handle)
{
    if (handle == NULL) {
        return;
    }
    if (handle->curl_handle == NULL) {
        return;
    }

    curl_easy_setopt(
        handle->curl_handle,
        CURLOPT_USERNAME,
        handle->username
        );
    curl_easy_setopt(
        handle->curl_handle,
        CURLOPT_PASSWORD,
        handle->password
        );
    curl_easy_setopt(
        handle->curl_handle,
        CURLOPT_HTTPAUTH,
        CURLAUTH_BASIC
        );
}


//Debug functions:

uint8_t friclient_dump_json(char* filename, cJSON* json)
{
    if ((filename == NULL) || (json == NULL)) {
        return 1;
    }
    char* string = cJSON_Print(json);

    FILE* f = fopen(filename, "w");
    if (f == NULL) {
        return 1;
    }

    size_t size = strlen(string);
    size_t written = fwrite(string, size, 1, f);
    fclose(f);

    //We only write a total of 1 block so we can simply check
    //if that one block has been written:
    if (written < 1) {
        return 1;
    }

    return 0;
}


//Initialisation and cleanup functions:


FriclientHandle* friclient_init(
    const char* handle_string
    )
{
    //We must split the handle_string into
    //separate username and domain:
    char* username = NULL;
    char* domain = NULL;
    size_t handle_length = strlen(handle_string);

    char* domain_start = strrchr(handle_string, '@') + 1;
    if (domain_start == NULL) {
        fprintf(
            stderr,
            "Invalid domain name!\n"
            );
        return NULL;
    }
    size_t domain_length = strlen(domain_start);
    domain = malloc(domain_length + 1);
    memset(domain, 0, domain_length + 1);
    memcpy(domain, domain_start, domain_length);

    //The username is (handle_length - domain_length - 1) bytes long:
    size_t username_length = handle_length - domain_length - 1;
    if (username_length < 1) {
        fprintf(
            stderr,
            "Invalid user name!\n"
            );
        return NULL;
    }
    username = malloc(username_length + 1);
    memset(username, 0, username_length + 1);
    memcpy(username, handle_string, username_length);

    CURL* curl_handle = curl_easy_init();
    if (curl_handle == NULL) {
        //Something went wrong:
        return NULL;
    }
    curl_easy_setopt(
        curl_handle,
        CURLOPT_WRITEFUNCTION,
        friclient_read_response
        );

    //Set the user agent:
    curl_easy_setopt(
        curl_handle,
        CURLOPT_USERAGENT,
        "Friclicli"
        );

    //Initialisation successful: We can build a friclient handle
    //with the curl handle and the other data.
    FriclientHandle* handle = malloc(sizeof(FriclientHandle));
    memset(handle, 0, sizeof(FriclientHandle));
    handle->curl_handle = curl_handle;

    //Domain and username are added:
    handle->domain = domain;
    handle->username = username;

    //Now we have to calculate the base URL:
    //The base URL starts with the string "https://", followed by the domain
    //and ends with the string "/api/".
    //Start and end strings are 13 characters in size.
    size_t base_url_size = 13 + strlen(handle->domain);
    handle->friendica_base_url = malloc(base_url_size + 1);
    if (handle->friendica_base_url == NULL) {
        //Error during memory allocation: abort.
        curl_easy_cleanup(curl_handle);
        friclient_destroy_handle(handle);
        return NULL;
    }
    sprintf(
        handle->friendica_base_url,
        "https://%s/api/",
        handle->domain
        );
    return handle;
}


//Functions for the Friendica API:


uint8_t friclient_get_server_info(FriclientHandle* handle)
{
    if (handle == NULL) {
        return 1;
    }

    cJSON* result = friclient_request_json(
        handle,
        FRICLIENT_HTTP_GET,
        "statusnet/version",
        NULL,
        0,
        NULL
        );
    if (result == NULL) {
        //Error during API call.
        #ifdef DEBUG
        fprintf(stderr, "DEBUG: Request result is null!\n");
        #endif
        return 1;
    }

    //Display the API version:
    //#ifdef DEBUG
    //fprintf(stderr, "Friendica API Version %s running on %s\n", result->valuestring, handle->friendica_base_url);
    //#endif
    if (handle->api_version) {
        free(handle->api_version);
    }
    //Set the API version to the friendica handle:
    size_t version_length = strlen(result->valuestring) + 1;
    handle->api_version = malloc(version_length);
    memset(handle->api_version, 0, version_length);
    strcpy(handle->api_version, result->valuestring);
}


uint8_t friclient_plain_login(
    FriclientHandle* handle
    )
{
    friclient_set_auth_credentials(handle);

    //Do the request:
    cJSON* result = friclient_request_json(
        handle,
        FRICLIENT_HTTP_GET,
        "account/verify_credentials",
        NULL,
        0,
        NULL
        );
    if (result == NULL) {
        //Error during API call.
        return 1;
    }

    //TODO: read the session cookie (if any) from curl.

    #ifdef DEBUG
    friclient_dump_json("../login-result.json", result);
    #endif

    cJSON* status = cJSON_GetObjectItemCaseSensitive(result, "status");
    if (status != NULL) {
        cJSON* error = cJSON_GetObjectItemCaseSensitive(status, "error");
        if (error != NULL) {
            //The login was not successful.
            free(status);
            return 1;
        }
    }

    //Get the information from the result.
    //The result is a JSON object containing a lot of information
    //regarding the user.
    //#ifdef DEBUG
    //fprintf(stderr, cJSON_Print(result));
    //#endif
    cJSON* uid = cJSON_GetObjectItemCaseSensitive(result, "id");
    if (uid != NULL) {
        #ifdef DEBUG
        fprintf(stderr, "uid = [%d]\n", uid->valueint);
        #endif
        handle->uid = uid->valueint;
    }
    #ifdef DEBUG
    if (result == NULL) {
        fprintf(stderr, "result is null!\n");
    }
    #endif
    cJSON* real_name = cJSON_GetObjectItemCaseSensitive(result, "screen_name");
    if (real_name != NULL) {
        #ifdef DEBUG
        fprintf(stderr, "name = [%s]\n", real_name->valuestring);
        #endif
        size_t real_name_length = strlen(real_name->valuestring);
        handle->real_name = malloc(real_name_length + 1);
        memset(handle->real_name, 0, real_name_length + 1);
        memcpy(handle->real_name, real_name->valuestring, real_name_length);
    }
    cJSON* timezone = cJSON_GetObjectItemCaseSensitive(result, "time_zone");
    if (timezone != NULL) {
        #ifdef DEBUG
        fprintf(stderr, "time_zone = [%s]\n", timezone->valuestring);
        #endif
        size_t timezone_length = strlen(timezone->valuestring);
        handle->timezone = malloc(timezone_length + 1);
        memset(handle->timezone, 0, timezone_length + 1);
        memcpy(handle->timezone, timezone->valuestring, timezone_length);
    }

    cJSON_Delete(result);

    return 0;
}


void friclient_logout(FriclientHandle* handle)
{
    //The Friendica API does not have a logout function at the moment.
    //Therefore we simply delete curl_password_string in the handle.

    if (handle->curl_password_string != NULL) {
        size_t password_length = strlen(handle->curl_password_string);
        memset(handle->curl_password_string, 0, password_length);
        free(handle->curl_password_string);
    }
}


void friclient_read_notifications(FriclientHandle* handle)
{
    friclient_set_auth_credentials(handle);

    cJSON* result = friclient_request_json(
        handle,
        FRICLIENT_HTTP_GET,
        "friendica/notifications",
        NULL,
        0,
        NULL
        );

    if (result == NULL) {
        //Error during API call.
        return;
    }

    #ifdef DEBUG
    friclient_dump_json("../get-notification-result.json", result);
    #endif

    if (!cJSON_IsArray(result)) {
        //The result format is wrong.
        return;
    }

    cJSON* current_notification = result;
    while (current_notification != NULL) {
        //Convert the notification data in an appropriate format
        //for furhter processing.
        current_notification = current_notification->next;
    }
}


FriclientPostCollection* friclient_read_timeline(
    FriclientHandle* handle,
    uint8_t include_private_posts
    )
{
    if (include_private_posts > 1) {
        //invalid number:
        include_private_posts = 0;
    }

    friclient_set_auth_credentials(handle);

    //Reserve space for one URL parameter as string:
    char** url_parameters = malloc(sizeof(char*));
    if (url_parameters == NULL) {
        return NULL;
    }
    //Allocate memory for the URL parameter and set it
    //to "exclude_replies=true" (20 characters + \0 byte).
    url_parameters[0] = malloc(21);
    if (url_parameters[0] == NULL) {
        free(url_parameters);
        return NULL;
    }
    strcpy(url_parameters[0], "exclude_replies=true");

    cJSON* result = friclient_request_json(
        handle,
        FRICLIENT_HTTP_GET,
        "statuses/home_timeline",
        url_parameters,
        1,
        NULL
        );

    //Free the memory for the url parameter:
    free(url_parameters[0]);
    free(url_parameters);

    //Process the result, which should be an array:
    if (!cJSON_IsArray(result)) {
        //The result is in the wrong format!
        cJSON_Delete(result);
        return NULL;
    }

    //Count the elements:
    size_t post_amount = cJSON_GetArraySize(result);

    FriclientPostCollection* collection = malloc(sizeof(FriclientPostCollection));
    memset(collection, 0, sizeof(FriclientPostCollection));

    if (post_amount == 0) {
        //Nothing to do.
        cJSON_Delete(result);
        return collection;
    }

    //Allocate memory in the post collection for the pointers
    //to the posts:
    collection->posts = malloc(sizeof(FriclientPost*) * post_amount);
    if (collection->posts == NULL) {
        cJSON_Delete(result);
        return collection;
    }

    //Set the size of the collection:
    collection->size = post_amount;

    //Now we loop over each JSON element and add its content
    //to the post collection. Memory for each post is allocated
    //in friclient_read_post_from_json.
    //We start at the first JSON array element:
    cJSON* current = result->child;
    size_t i = 0;
    while (current != NULL) {
        FriclientPost* post = friclient_read_post_from_json(current);

        if (post != NULL) {
            collection->posts[i] = post;
            //Increment the collection element counter:
            i++;
        }
        //Go to the next JSON array element:
        current = current->next;
    }

    //At the end we have the amount of successfully read posts stored in i.
    //TODO: Check if it is ok when the previous collection size is bigger than i.
    //In that case there are some "empty" pointers at the end of the array.
    collection->size = i;

    //cJSON_Delete deletes recursively all of the JSON structure:
    cJSON_Delete(result);

    return collection;
}


FriclientPost* friclient_read_post(
    FriclientHandle* handle,
    const char* post_id,
    uint8_t with_conversations
    )
{
    if (handle == NULL) {
        return NULL;
    }
    if (post_id == NULL) {
        return NULL;
    }

    size_t parameter_count = 1;
    if (with_conversations > 0) {
        parameter_count++;
    }

    //Reserve space for_one URL parameter as string:
    char** url_parameters = malloc(sizeof(char*) * parameter_count);
    if (url_parameters == NULL) {
        return NULL;
    }
    //Allocate memory for the "id" parameter:
    //"id=" + id-string + null characeter
    size_t id_param_size = strlen(post_id) + 4;
    url_parameters[0] = malloc(id_param_size);
    if (url_parameters[0] == NULL) {
        free(url_parameters);
        return NULL;
    }
    memset(url_parameters[0], 0, id_param_size);
    sprintf(
        url_parameters[0],
        "id=%s",
        post_id
        );

    if (with_conversations > 0) {
        //Add a second URL parameter with the content "conversation=1":
        url_parameters[1] = malloc(15);
        if (url_parameters[1] == NULL) {
            free(url_parameters[0]);
            free(url_parameters);
            return NULL;
        }
        memset(url_parameters[1], 0, 15);
        strcpy(url_parameters[1], "conversation=1");
    }

    friclient_set_auth_credentials(handle);

    cJSON* result = friclient_request_json(
        handle,
        FRICLIENT_HTTP_GET,
        "statuses/show",
        url_parameters,
        parameter_count,
        NULL
        );

    //The URL parameters aren't needed anymore:
    size_t i = 0;
    for (i = 0; i < parameter_count; i++) {
        free(url_parameters[i]);
    }
    free(url_parameters);

    if (!cJSON_IsObject(result)) {
        cJSON_Delete(result);
        return NULL;
    }

    FriclientPost* post = friclient_read_post_from_json(result);

    cJSON_Delete(result);

    //Either a post is returned or NULL, depending on what
    //friclien_read_post_from_json could do with the JSON data.
    return post;
}


FriclientPost* friclient_send_post(
    FriclientHandle* handle,
    const char* title,
    const char* text,
    uint64_t* recipient_user_ids
    )
{
    if (handle == NULL) {
        //Nothing to do.
        return NULL;
    }

    size_t title_length = 0;
    size_t text_length = 0;
    if (title != NULL) {
        title_length = strlen(title);
    }
    if (text != NULL) {
        text_length = strlen(text);
    }
    if (text_length < 1) {
        //Still nothing to do.
        return NULL;
    }

    //Build the POST fields:

    size_t field_count = 2;
    if (title_length > 0) {
        field_count++;
    }

    const char* source_field = "source=Friclicli";

    char* escaped_text = curl_easy_escape(handle->curl_handle, text, text_length);
    size_t escaped_text_length = strlen(escaped_text);

    //The title is optional, the text not.
    //Since the "existence" of text is checked at the beginning
    //we must only check for the existence of title here.
    char* escaped_title = NULL;
    size_t escaped_title_length = 0;
    if (title != NULL) {
        escaped_title = curl_easy_escape(handle->curl_handle, title, title_length);
        escaped_title_length = strlen(escaped_title);
    }

    FriclientData* post_fields = malloc(sizeof(FriclientData));
    if (post_fields == NULL) {
        return NULL;
    }

    //Explaination of the size of field_length:
    //"status=(content of escaped text)&source=Friclicli\0".
    size_t fields_length = 8 + escaped_text_length
        + strlen(source_field) + 2;
    if (escaped_title_length > 0) {
        //Explaination:
        //"&title=(content of escaped title)"
        fields_length += 7 + escaped_title_length;
    }
    post_fields->data = malloc(fields_length);
    if (post_fields->data == NULL) {
        free(post_fields);
        return NULL;
    }
    memset(post_fields->data, 0, fields_length);

    post_fields->size = fields_length;
    post_fields->allocated_size = fields_length;

    strcat(post_fields->data, source_field);
    strcat(post_fields->data, "&status=");
    strcat(post_fields->data, escaped_text);
    if (escaped_title_length > 0) {
        strcat(post_fields->data, "&title=");
        strcat(post_fields->data, escaped_title);
    }

    #ifdef DEBUG
    fprintf(
        stderr,
        "post_fields->data = \n%s",
        post_fields->data
        );
    #endif

    //Now we can call the API:
    cJSON* result = friclient_request_json(
        handle,
        FRICLIENT_HTTP_POST,
        "statuses/update",
        NULL,
        0,
        post_fields
        );

    if (post_fields) {
        if (post_fields->data) {
            free(post_fields->data);
        }
        free(post_fields);
    }

    #ifdef DEBUG
    friclient_dump_json("../send_post_result.json", result);
    #endif

    //The result should be a Friendica post.
    return friclient_read_post_from_json(result);
}
