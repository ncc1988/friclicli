/*
    This file is part of libfriclient.
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "friclient_handle.h"


void friclient_destroy_handle(FriclientHandle* handle)
{
    if (handle == NULL) {
        //Nothing to do.
        return;
    }

    //username and password are sensitive information.
    //If the curl_password_string field is set it is zeroed first
    //before the memory is freed:
    if (handle->curl_password_string != NULL) {
        size_t curl_password_length = strlen(handle->curl_password_string);
        memset(handle->curl_password_string, 0, curl_password_length);
        curl_password_length = 0;
        free(handle->curl_password_string);
    }
    if (handle->domain != NULL) {
        size_t domain_length = strlen(handle->domain);
        memset(handle->domain, 0, domain_length);
        free(handle->domain);
    }
    if (handle->username != NULL) {
        size_t uname_length = strlen(handle->username);
        memset(handle->username, 0, uname_length);
        free(handle->username);
    }
    if (handle->password != NULL) {
        size_t pw_length = strlen(handle->password);
        memset(handle->password, 0, pw_length);
        pw_length = 0;
        free(handle->password);
    }
    if (handle->api_version != NULL) {
        size_t apiver_length = strlen(handle->api_version);
        memset(handle->api_version, 0, apiver_length);
        free(handle->api_version);
    }
    if (handle->friendica_base_url != NULL) {
        size_t url_length = strlen(handle->friendica_base_url);
        memset(handle->friendica_base_url, 0, url_length);
        free(handle->friendica_base_url);
    }
    if (handle->curl_handle != NULL) {
        curl_easy_cleanup(handle->curl_handle);
    }

    //After we freed the memory of all fields in the struct,
    //we can free the struct itself:
    free(handle);
}
