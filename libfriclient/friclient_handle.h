/*
    This file is part of libfriclient.
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FRICLIENT_HANDLE__H
#define FRICLIENT_HANDLE__H


#include <malloc.h>
#include <inttypes.h>
#include <string.h>


#include <curl/curl.h>


typedef struct {
    CURL* curl_handle;
    char* friendica_base_url;
    char* domain;
    char* username;
    char* password;
    char* curl_password_string; //TODO: check if it is secure to store the password in plaintext in memory!
    char* api_version; ///The API version used on the remote friendica instance.

    //"Metadata":
    uint64_t uid;
    char* real_name;
    char* timezone;
}FriclientHandle;


/**
 * Destroys the handle securely (hopefully securely, to be verified!).
 */
void friclient_destroy_handle(FriclientHandle* handle);


#endif
