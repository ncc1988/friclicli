/*
    This file is part of libfriclient.
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FRICLIENT_POST__H
#define FRICLIENT_POST__H


#include <malloc.h>
#include <inttypes.h>
#include <string.h>


#include "cJSON/cJSON.h"


#include "friclient_user.h"


typedef struct
{
    uint64_t uid;
    char* verb;
}FriclientPostReaction;


struct FriclientPost
{
    uint64_t post_id;
    FriclientUser* user; //The user who made the post.
    FriclientUser* owner; //(probably) the user who made the original post.
    uint8_t is_public;
    char* text;
    size_t text_size;
    char* html_text;
    size_t html_size;
    FriclientPostReaction** reactions;
    size_t reactions_size;
    struct FriclientPost** replies;
    size_t replies_size;
    struct FriclientPost** reshares;
    size_t reshares_size;
};
typedef struct FriclientPost FriclientPost;


typedef struct
{
    FriclientPost** posts;
    size_t size;
}FriclientPostCollection;


//Functions related to the FriclientPost structure:


FriclientPost* friclient_read_post_from_json(cJSON* json);


void friclient_free_post_reaction(FriclientPostReaction* reaction);


void friclient_free_post(FriclientPost* post);


void friclient_free_post_collection(FriclientPostCollection* collection);


//FriclientPost service functions:

/**
 * Counts the likes from a post.
 */
uint16_t friclient_post_count_likes(FriclientPost* post);


/**
 * Counts the dislikes from a post.
 */
uint16_t friclient_post_count_dislikes(FriclientPost* post);


/**
 * Counts the reshares of a post.
 */
uint16_t friclient_post_count_reshares(FriclientPost* post);


/**
 * Counts the direct comments of a post.
 * Note that this only retrieves the direct comments,
 * not the comments of all subthreads.
 */
uint16_t friclient_post_count_comments(FriclientPost* post);


/**
 * Sets the text of the FriclientPost.
 */
void friclient_post_set_text(FriclientPost* post, const char* text);


/**
 * Sets the text of the FriclientPost.
 */
void friclient_post_set_html_text(FriclientPost* post, const char* html_text);


#endif
