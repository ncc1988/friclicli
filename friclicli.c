/*
    friclicli
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <curl/curl.h>
#include <ncursesw/curses.h>

#include "friclicli_compile_config.h"

#include "libfriclient/libfriclient.h"
#include "friclicli_user_config.h"
#include "gui/gui_common.h"
#include "gui/login_window.h"
#include "gui/main_window.h"


void print_help()
{
    printf("Typical usage: friclicli [USERNAME@FRIENDICA_INSTANCE]\n\n");
    printf("Parameters:\n");
    printf("USERNAME is your Friendia user name while\n");
    printf("FRIENDICA_INSTANCE is the domain of your friendica instance.\n\n");
    printf("These parameters are only needed if no configuration file exists\n"
           "or if you want to log in with another profile.\n\n");
}


FriclicliConfig* read_config()
{
    char* home_dir = getenv("HOME");
    if (home_dir == NULL) {
        fprintf(
            stderr,
            "Cannot determine home directory!\n"
            );
        return NULL;
    }

    size_t home_length = strlen(home_dir);
    //Reserve enough space for "$HOME/.config/friclicli":
    char* config_file_path = malloc(home_length + 19);
    memset(config_file_path, 0, home_length + 19);
    strncpy(config_file_path, home_dir, home_length);
    strcat(config_file_path, "/.config/friclicli");

    FriclicliConfig* config = friclicli_config_read(config_file_path);
    //We don't have any other configuration files
    //we can probe at the moment.
    return config;
}


void init_gui()
{
    initscr();
    cbreak();
    keypad(stdscr, TRUE);
    clear();
}


void shutdown_gui()
{
    endwin();
}


int main(int argc, char** argv)
{
    char* handle_string = NULL;

    FriclicliConfig* config = NULL;

    if (argc < 2) {
        config = read_config();
        if (config == NULL) {
            fprintf(
                stderr,
                "No configuration file found!\n"
                );
            return 1;
        }
        if ((config->username == NULL) || (config->host == NULL)) {
            fprintf(
                stderr,
                "No username or host are set in the configuration file!\n"
                );
            return 1;
        }

        //Temporary solution: create a handle string from the configuration.
        size_t handle_length = strlen(config->username)
            + strlen(config->host) + 2;
        handle_string = malloc(handle_length);
        if (handle_string == NULL) {
            fprintf(
                stderr,
                "Memory allocation error!\n"
                );
            return 1;
        }
        sprintf(
            handle_string,
            "%s@%s",
            config->username,
            config->host
            );
    } else {
        size_t handle_length = 0;
        handle_length = strlen(argv[1]);
        handle_string = malloc(handle_length + 1);
        memset(handle_string, 0, handle_length + 1);
        strcpy(handle_string, argv[1]);
    }

    curl_global_init(CURL_GLOBAL_DEFAULT);

    //Init the GUI:
    init_gui();
    int gui_width = 0;
    int gui_height = 0;
    FriclicliGui* gui = malloc(sizeof(FriclicliGui));
    if (gui == NULL) {
        fprintf(stderr, "Error initialising GUI!\n");
        return 1;
    }
    memset(gui, 0, sizeof(FriclicliGui));
    gui_init(gui);
    gui_setup(gui);

    //Create a Friclient handle:
    FriclientHandle* handle = friclient_init(handle_string);
    if (handle == NULL) {
        curl_global_cleanup();
        fprintf(stderr, "Error initialising friclient handle!\n");
        return 1;
    }

    //At this point we don't need handle_string anymore:
    free(handle_string);

    //Get server information:
    friclient_get_server_info(handle);

    //Fill the navigation bar:
    gui_set_nav_bar(
        gui,
        "Friclicli login"
        );

    //Show the login window:
    login_window_show(gui, handle);

    //Show the main window after the login window has finished:
    main_window_show(gui, handle);

    //If code execution reaches this point the user pressed
    //the key for quitting friclicli.
    //Start the shutdown procedure:
    friclient_destroy_handle(handle);
    gui_delete(gui);

    //Final shutdown functions:
    shutdown_gui();
    curl_global_cleanup();

    //Be kind:
    printf("Friclicli has shut down. Have a nice day!\n");

    return 0;
}
