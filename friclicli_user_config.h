/*
    friclicli
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FRICLICLI_USER_CONFIG__H
#define FRICLICLI_USER_CONFIG__H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <inttypes.h>


//This file contains data structures and functions for handling
//the individual configuration of users.


struct FriclicliConfig
{
    char* username;
    char* password;
    char* host;

    /**
     * Whether to use the "mail client view" for posts
     * which make the timeline view look similar to
     * the mail box view of mutt / neomutt.
     * If set to 1 the "mail client view" is used.
     * In other cases, the "web view" is used which displays
     * posts more like the Friendica web interface.
     */
    uint8_t use_mailclient_view;

    //The following is reserved for optional configuration entries.
    char** keys;
    char** values;
    size_t entries;
};
typedef struct FriclicliConfig FriclicliConfig;


FriclicliConfig* friclicli_config_read(const char* filename);


void friclicli_config_write(FriclicliConfig* config);


void friclicli_config_free_keys(
    FriclicliConfig* config
    );


void friclicli_config_free_values(
    FriclicliConfig* config
    );


char* friclicli_config_get_value(
    FriclicliConfig* config,
    const char* name
    );

void friclicli_config_set_value(
    FriclicliConfig* config,
    const char* name,
    const char* value
    );


#endif
