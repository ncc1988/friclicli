/*
    This file is part of friclicli.
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "message_window.h"


void message_window_show(
    FriclicliGui* gui,
    FriclientHandle* handle
    )
{
    //No message text has been written before.
    //Switch directly to the edit function.
    FriclicliMessageData* message = message_window_edit(
        gui,
        handle,
        NULL
        );

    gui_set_nav_bar(
        gui,
        "\t(y) Send message\t(e) Edit message\t(q) Discard message"
        );
    //Clear the content:
    gui_set_content(
        gui,
        ""
        );

    message_window_print_message_text(gui, message);

    int32_t selection = 0;
    do {
        selection = getch();

        switch (selection) {
            case 'e': {
                message = message_window_edit(gui, handle, message);
                message_window_print_message_text(gui, message);
                break;
            }
            case 't': {
                message = message_window_set_title(gui, message);
                message_window_print_message_text(gui, message);
                break;
            }
            case 'y': {
                if (message == NULL)  {
                    gui_set_bottom_bar(gui, "The message is empty!");
                } else {
                    //Send message:
                    FriclientPost* post = friclient_send_post(
                        handle,
                        message->title,
                        message->text,
                        NULL
                        );
                    if (post != NULL) {
                        //Show a success message:
                        gui_set_bottom_bar(gui, "Message sent!");
                        free(post);
                    }
                    return;
                }
            }
            case 'q': {
                //Discard the message:
                if (message != NULL) {
                    if (message->text != NULL) {
                        free(message->text);
                    }
                    free(message);
                }
                return;
            }
            default: {
                gui_set_bottom_bar(
                    gui,
                    "The pressed key is not assigned to a command."
                    );
            }
        }
    } while (1);
}


FriclicliMessageData* message_window_edit(
    FriclicliGui* gui,
    FriclientHandle* handle,
    FriclicliMessageData* message
    )
{
    //Create a temporary file:
    //TODO: Do not hard code the editor and /tmp as directory for the file!
    char* file_name = malloc(16); //path to file: "/tmp/" + 10 Bytes + null character.
    if (!file_name) {
        return NULL;
    }
    memset(file_name, 0, 16);
    sprintf(
        file_name,
        "/tmp/%u",
        rand()
        );

    char* editor_command = malloc(19); //19 = file _name + "vi ".
    if (editor_command == NULL) {
        return NULL;
    }
    memset(editor_command, 0, 19);
    sprintf(
        editor_command,
        "vi %s",
        file_name
        );

    if (message != NULL) {
        //This function is called with existing message data.
        //We must write the message text to the temporary file
        //for editing it:
        if (message->text != NULL) {
            FILE* f = fopen(file_name, "w");
            if (f != NULL) {
                fwrite(message->text, message->text_length, 1, f);
                fclose(f);
            }
        }
    }

    //Leave curses mode:
    def_prog_mode();
    endwin();

    //Start the text editor and wait until it returns:
    system(editor_command);
    free(editor_command);

    //Restore the curses gui:
    reset_prog_mode();
    refresh();
    wrefresh(gui->content);

    //Now we read the file's content:
    char* buffer = malloc(65536); //We read the content in 64 KiB blocks.
    if (!buffer) {
        free(file_name);
        return NULL;
    }
    memset(buffer, 0, 65536);

    FILE* f = fopen(file_name, "r");

    if (f == NULL) {
        remove(file_name);
        free(buffer);
        free(file_name);
        return NULL;
    }

    //Read the content.
    //TODO: remove 64 KiB limit!
    fread(buffer, 65535, 1, f);
    fclose(f);

    //file_name and the temporary file aren't needed anymore:
    remove(file_name);
    free(file_name);

    size_t buffer_length = strlen(buffer);

    if (buffer_length == 0) {
        //Empty post: Abort, return to main view
        //but display a message.
        free(buffer);

        gui_set_bottom_bar(gui, "Empty message body: abort!");

        return NULL;
    }

    if (message == NULL) {
        message = malloc(sizeof(FriclicliMessageData));
        if (message == NULL) {
            free(buffer);
            gui_set_bottom_bar(gui, "Memory allocation error!");
            return NULL;
        }
        memset(message, 0, sizeof(FriclicliMessageData));
    }

    if (message->text != NULL) {
        free(message->text);
        message->text_length = 0;
    }

    //Copy the content of buffer into the message structure.
    //Buffer may be a lot bigger than the text so we can save memory
    //by allocating only the amount of memory we need.
    message->text = malloc(buffer_length + 1);
    if (message->text == NULL) {
        free(buffer);
        gui_set_bottom_bar(gui, "Memory allocation error!");
        return NULL;
    }
    memset(message->text, 0, buffer_length + 1);
    strcpy(message->text, buffer);
    message->text_length = buffer_length;
    free(buffer);

    return message;
}


FriclicliMessageData* message_window_set_title(
    FriclicliGui* gui,
    FriclicliMessageData* message
    )
{
    if (message == NULL) {
        gui_set_bottom_bar(
            gui,
            "Invalid message structure in program code!"
            );
        return message;
    }

    //Early devlopment code: Limit the title to 160 characters:
    size_t title_limit = 161;
    char* title = malloc(title_limit);
    if (title == NULL) {
        gui_set_bottom_bar(
            gui,
            "Memory allocation error for title!"
            );
        return message;
    }

    if (message->title != NULL) {
        //Use the title that is currently set and make sure
        //it is null terminated:
        strncpy(title, message->title, title_limit - 1);
        title[title_limit - 1] = 0;
    } else {
        //Make the string "empty" (null terminated):
        memset(title, 0, title_limit);
    }

    gui_set_bottom_bar(
        gui,
        "Title: "
        );

    int32_t selection = 0;
    selection = getch();
    size_t i = strlen(title);
    while(selection != '\n') {
        if (selection == 7) { //Backspace
            title[i] = 0;
            i--;
            if (i < 0) {
                i = 0;
            }
        } else {
            if (i < title_limit) {
                //TODO: allow unicode characters here!
                title[i] = selection;
            } else {
                gui_set_bottom_bar(
                    gui,
                    "Maximum title size reached!"
                    );
                break;
            }
            i++;
        }
        selection = getch();
    }

    size_t title_length = strlen(title);
    if (title_length > 0) {
        message->title = realloc(message->title, title_length + 1);
        if (message->title == NULL) {
            message->title_length = 0;
            gui_set_bottom_bar(
                gui,
                "Cannot store title: Memory reallocation error!"
                );
            free(title);
            return message;
        }
        memset(message->title, 0, title_length + 1);
        strcpy(message->title, title);
        message->title_length = title_length;
    } else {
        if (message->title != NULL) {
            free(message->title);
            message->title_length = 0;
        }
        gui_set_bottom_bar(
            gui,
            "Title removed!"
            );
     }
    free(title);
    return message;
}


void message_window_print_message_text(
    FriclicliGui* gui,
    FriclicliMessageData* message
    )
{
    //EARLY DEVELOPMENT STAGE CODE:
    //Show a warning above the message text that only public posts
    //are supported at the moment.

    //MORE EARLY DEVELOPMENT STAGE CODE:
    //Use a ncurses window for now.
    //TODO: Switch to ncurses pads.

    if (message == NULL) {
        return;
    }

    char* info_text = malloc(255);
    memset(info_text, 0, 255);
    sprintf(
        info_text,
        "Message size: %d characters",
        message->text_length
        );

    gui_set_info_bar(
        gui,
        info_text
        );
    free(info_text);

    attron(COLOR_PAIR(2));
    mvwprintw(
        gui->content,
        0,
        0,
        "--------\nWARNING: This feature is in development.\nTo send the text below as *public* post, press Y. To edit the text, press E. To add a title, press T. To abort, press Q.\n--------\n"
        );
    if (message->title != NULL) {
        wprintw(gui->content, "----\nMessage title: ");
        wprintw(gui->content, message->title);
        wprintw(gui->content, "\n----\n");
    }
    wprintw(
        gui->content,
        message->text
        );
    attroff(COLOR_PAIR(2));
    wrefresh(gui->content);
}
