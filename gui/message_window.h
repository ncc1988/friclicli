/*
    This file is part of friclicli.
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MESSAGE_WINDOW__H
#define MESSAGE_WINDOW__H


#include <stdio.h>
#include <stdlib.h>
#include <ncursesw/curses.h>


#include "gui_common.h"
#include "../libfriclient/libfriclient.h"


typedef struct {
    char* text;
    size_t text_length;
    char* title;
    size_t title_length;
}FriclicliMessageData;


void message_window_show(
    FriclicliGui* gui,
    FriclientHandle* handle
    );


FriclicliMessageData* message_window_edit(
    FriclicliGui* gui,
    FriclientHandle* handle,
    FriclicliMessageData* message
    );


FriclicliMessageData* message_window_set_title(
    FriclicliGui* gui,
    FriclicliMessageData* message
    );


void message_window_print_message_text(
    FriclicliGui* gui,
    FriclicliMessageData* message
    );

#endif
