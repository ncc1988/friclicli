/*
    friclicli
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "gui_common.h"


void gui_init(FriclicliGui* gui)
{
    if (gui == NULL) {
        gui = malloc(sizeof(FriclicliGui));
        memset(gui, 0, sizeof(FriclicliGui));
    }

    getmaxyx(stdscr, gui->height, gui->width);
    start_color(); //Colour output
    curs_set(0); //Invisible cursor
}


void gui_delete(FriclicliGui* gui)
{
    if (gui == NULL) {
        return;
    }

    if (gui->content == NULL) {
        return;
    }

    wborder(gui->content, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    delwin(gui->content);

    free(gui);
}


void gui_init_content(FriclicliGui* gui)
{
    if (gui == NULL) {
        return;
    }

    gui->content = newwin(
        gui->height - 4,
        gui->width,
        1,
        1
        );
    scrollok(gui->content, TRUE);
    wrefresh(gui->content);
}


void gui_setup(FriclicliGui* gui)
{
    if (gui == NULL) {
        return;
    }

    gui->info_row_num = gui->height - 3;
    gui_init_content(gui);

    //Define colour pairs:
    //Navigation bar:
    init_pair(1, COLOR_GREEN, COLOR_BLUE);
    //Content:
    init_pair(2, COLOR_WHITE, COLOR_RED);
    //Info bar:
    init_pair(3, COLOR_GREEN, COLOR_BLUE);
    //Status bar:
    init_pair(4, COLOR_YELLOW, COLOR_BLACK);

    //Set the colors for the bars
    //and the content area:

    move(0, 0);
    attron(COLOR_PAIR(1));
    hline(' ', gui->width);
    attroff(COLOR_PAIR(1));

    /*
    uint16_t i = 0;
    attron(COLOR_PAIR(2));
    for (i = 0; i < gui->height - 4; i++) {
        wmove(gui->content, i, 0);
        hline(' ', gui->width);
    }
    attroff(COLOR_PAIR(2));
    */

    move(gui->info_row_num, 0);
    attron(COLOR_PAIR(3));
    hline(' ', gui->width);
    attroff(COLOR_PAIR(3));

    move(gui->info_row_num + 1, 0);
    attron(COLOR_PAIR(4));
    hline(' ', gui->width);
    attroff(COLOR_PAIR(4));

    refresh();
}


void gui_set_nav_bar(FriclicliGui* gui, char* content)
{
    move(0, 0);
    attron(COLOR_PAIR(1) | A_BOLD);
    hline(' ', gui->width);
    mvprintw(0, 0, content);
    attroff(COLOR_PAIR(1));
}


void gui_set_content(FriclicliGui* gui, char* content)
{
    if (gui == NULL) {
        return;
    }

    if (gui->content == NULL) {
        gui_init_content(gui);
    }

    attron(COLOR_PAIR(2));
    mvwprintw(gui->content, 0, 0, content);
    attroff(COLOR_PAIR(2));
    /*
    //Fill the lines until the end of the content with emptyness:
    //Count the lines in content:

    int current_height = 0;
    int current_width = 0;
    getyx(gui->content, current_height, current_width);
    int content_height = 0;
    int content_width = 0;
    getmaxyx(gui->content, content_height, content_width);

    while (current_height < content_height) {
        whline(gui->content, ' ', content_width);
        current_height++;
    }
    */

    //Finally do a refresh:
    wrefresh(gui->content);
}


void gui_set_info_bar(FriclicliGui* gui, char* content)
{
    move(gui->info_row_num, 0);
    attron(COLOR_PAIR(3 | A_BOLD));
    hline(' ', gui->width);
    mvprintw(gui->info_row_num, 0, content);
    attroff(COLOR_PAIR(3));
}


void gui_set_bottom_bar(FriclicliGui* gui, char* content)
{
    move(gui->info_row_num + 1, 0);
    attron(COLOR_PAIR(4));
    hline(' ', gui->width);
    mvprintw(gui->info_row_num + 1, 0, content);
    attroff(COLOR_PAIR(4));
}


void gui_content_next_line(FriclicliGui* gui)
{
    
}


void gui_content_previous_line(FriclicliGui* gui)
{
    
}


void gui_remove_bad_characters(char* text_line)
{
    if (text_line == NULL) {
        return;
    }

    size_t i = 0;
    for (i = 0; text_line[i] != '\0'; i++) {
        if (text_line[i] < 0x20) {
            //Replace all control characters with a space character:
            text_line[i] = 0x20;
        }
    }
}
