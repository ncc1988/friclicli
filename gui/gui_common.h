/*
    friclicli
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUI_COMMON__H
#define GUI_COMMON__H


//#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <ncursesw/curses.h>


/**
 * The GUI is split into four "areas":
 * - navigation bar (at the top)
 * - content
 * - info bar (information)
 * - bottom bar (for input and status messages)
 */
typedef struct
{
    int width;
    int height;

    /**
     * The content area is the only window
     * used in this GUI.
     */
    WINDOW* content;
    /**
     * The row number of the info bar.
     * Using this number the height
     * of the content can be calculated
     * as info_row_num - 2.
     * The bottom bar starts at info_row_num + 1.
     */
    int info_row_num;
}FriclicliGui;



void gui_init(FriclicliGui* gui);


void gui_delete(FriclicliGui* gui);


void gui_init_content(FriclicliGui* gui);


void gui_setup(FriclicliGui* gui);


void gui_set_nav_bar(FriclicliGui* gui, char* content);


void gui_set_content(FriclicliGui* gui, char* content);


void gui_set_info_bar(FriclicliGui* gui, char* content);


void gui_set_bottom_bar(FriclicliGui* gui, char* content);


void gui_content_next_line(FriclicliGui* gui);


void gui_content_previous_line(FriclicliGui* gui);


void gui_remove_bad_characters(char* text_line);


#endif
