/*
    friclicli
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "main_window.h"


void main_window_restore(FriclicliGui* gui, FriclientHandle* handle)
{
    gui_set_nav_bar(
        gui,
        "\t(t) timeline*\t(m) send message/post\t(n) notifications\t(p) private messages\t(c) contacts\t"
        );
    gui_set_content(
        gui,
        ""
        );

    if (handle != NULL) {
        //Display some server information in the info bar:
        char* info_text = malloc(gui->width);
        if (info_text != NULL) {
            //TODO: Make sure that the info text cannot be longer than gui->width!
            sprintf(
                info_text,
                "User: [%s] Server: [%s] API version: %s",
                (
                    handle->username != NULL
                    ? handle->username
                    : ""
                    ),
                (
                    handle->friendica_base_url != NULL
                    ? handle->friendica_base_url
                    : ""
                    ),
                (
                    handle->api_version != NULL
                    ? handle->api_version
                    : ""
                    )
                );
            gui_set_info_bar(gui, info_text);
            refresh();
            free(info_text);
        }
    }
}

void main_window_show(FriclicliGui* gui, FriclientHandle* handle)
{
    main_window_restore(gui, handle);

    main_window_show_posts(gui, handle);
    refresh();

    int32_t selection = 0;
    do {
       selection = getch();

        switch (selection) {
            case KEY_DOWN: {
                gui_content_next_line(gui);
                break;
            }
            case KEY_UP: {
                gui_content_previous_line(gui);
                break;
            }
            case 'p': {
                //profile_window_show(gui, handle);
                break;
            }
            case 'n': {
                //notification_window_show(gui, handle);
                break;
            }
            case 'm': {
                message_window_show(gui, handle);
                main_window_restore(gui, handle);
                main_window_show_posts(gui, handle);
                //TODO: add a cache to avoid reloading the timeline every time
                //every time a message is sent.
                break;
            }
            case 'c': {
                //contact_window_show(gui, handle);
                break;
            }
            case '$': {
                main_window_show_posts(gui, handle);
                gui_set_bottom_bar(
                    gui,
                    "Timeline reloaded."
                    );
                break;
            }
            default: {
                gui_set_bottom_bar(
                    gui,
                    "The pressed key is not assigned to a command."
                    );
            }
        }
        refresh();
    } while (selection != 'q');
}


/**
 * This shows all posts from the logged in user and zis contacts.
 */
void main_window_show_posts(FriclicliGui* gui, FriclientHandle* handle)
{
    FriclientPostCollection* timeline = friclient_read_timeline(
        handle,
        1
        );

    if (timeline == NULL) {
        gui_set_bottom_bar(
            gui,
            "There are no posts to be displayed."
            );
        return;
    }

    //The main GUI shall display the posts in the following
    //table format (example):
    //  L  |  D  |  R  |  C  | Content
    //  14 |   1 |  23 | 180 | This is a test post from friclicli. It is...
    //
    //Explaination: L = likes, D = dislikes, R = reshares, C = comments

    int content_height = gui->info_row_num - 2;
    size_t lines_to_draw = content_height;

    if (content_height > timeline->size) {
        lines_to_draw = timeline->size;
    }

    if (lines_to_draw < 1) {
        //There is nothing to draw. We can stop here:
        free(timeline);
        return;
    }

    //content size = width * height + newline for each line + null character.
    size_t content_size = ((gui->width + 1) * lines_to_draw) + 1;
    char* content = malloc(content_size);
    memset(content, 0, content_size);
    sprintf(
        content,
        "  L  |  D  |  R  |  C  | Author                   | Content\n"
        );
    #ifdef DEBUG
    fprintf(
        stderr,
        "  L  |  D  |  R  |  C  | Author                   | Content\n"
        );
    #endif
    size_t shortened_text_max_length = gui->width - 44;
    char* shortened_text = malloc(shortened_text_max_length + 1);
    memset(shortened_text, 0, shortened_text_max_length + 1);

    char* shortened_author = malloc(25);
    memset(shortened_author, 0, 25);

    size_t i = 0;
    char* write_pos = content;
    for (i = 0; i < lines_to_draw; i++) {
        if (timeline->posts[i] != NULL) {
            FriclientPost* current_post = timeline->posts[i];
            size_t post_visible_length = shortened_text_max_length - 3;
            if (current_post->text_size - 3 < post_visible_length) {
                post_visible_length = current_post->text_size - 3;
            }
            strncpy(shortened_text, current_post->text, post_visible_length);
            gui_remove_bad_characters(shortened_text);

            //Get the author's name and shorten it:
            char* author_name = friclient_user_get_name_or_id(
                current_post->user
                );
            if (author_name != NULL) {
                size_t author_size = strlen(author_name);
                if (author_size > 24) {
                    author_size = 24;
                }
                strncpy(
                    shortened_author,
                    author_name,
                    author_size
                );
            }
            free(author_name);
            //TODO: add ... to end of shortened text
            write_pos += sprintf(
                write_pos,
                "%4d |%4d |%4d |%4d | %24s | %s\n",
                friclient_post_count_likes(current_post),
                friclient_post_count_dislikes(current_post),
                friclient_post_count_reshares(current_post),
                friclient_post_count_comments(current_post),
                shortened_author,
                shortened_text
                );
            #ifdef DEBUG
            fprintf(
                stderr,
                "%4d |%4d |%4d |%4d | %24s | %s\n",
                friclient_count_likes(current_post),
                friclient_count_dislikes(current_post),
                friclient_count_reshares(current_post),
                friclient_count_comments(current_post),
                shortened_author,
                shortened_text
                );
            #endif
            memset(shortened_author, 0, 25);
            memset(shortened_text, 0, shortened_text_max_length + 1);
        }
    }

    free(shortened_author);
    free(shortened_text);

    gui_set_content(gui, content);

    memset(content, 0, content_size);
    free(content);

    friclient_free_post_collection(timeline);
}
