/*
    friclicli
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "login_window.h"



void login_window_show(FriclicliGui* gui, FriclientHandle* handle)
{
    if ((gui == NULL) || (handle == NULL)) {
        return;
    }

    //Ask for the password:
    gui_set_bottom_bar(
        gui,
        "Please enter your Friendica password:"
        );

    //Do not echo password on the screen!!
    noecho();

    size_t pw_max_size = 256; //This is poor and should be improved!
    size_t pw_pos = 0; //The current position in the password string.
    char* password = malloc(pw_max_size + 1);
    memset(password, 0, pw_max_size + 1);
    char current_char = getch();
    while ((current_char != '\n') && (pw_pos < pw_max_size)) {
        if (current_char == 7) { //Backspace
            password[pw_pos] = '\0';
            pw_pos--;
            if (pw_pos < 0) {
                pw_pos = 0;
            }
        } else {
            password[pw_pos] = current_char;
            pw_pos++;
        }
        current_char = getch();
    }

    //After we got the password we can echo again:
    echo();

    //Now we store the password in the Friclient handle:
    if (handle->password) {
        free(handle->password);
    }
    //pw_pos points to the last character of the password.
    handle->password = malloc(pw_pos + 1);
    memset(handle->password, 0, pw_pos + 1);
    memcpy(handle->password, password, pw_pos);

    //Now we try to login:

    gui_set_bottom_bar(gui, "Login...");
    if (friclient_plain_login(handle) == 0) {
        gui_set_bottom_bar(gui, "Login successful!");
        char* info_bar_text = malloc(gui->width);
        memset(info_bar_text, 0, gui->width);
        if (info_bar_text) {
            sprintf(
                info_bar_text,
                "Welcome %s",
                handle->real_name
                );
            gui_set_info_bar(gui, info_bar_text);
        }
    } else {
        gui_set_bottom_bar(gui, "Login failed!");
        refresh();
    }
}
