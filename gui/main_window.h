/*
    friclicli
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MAIN_WINDOW__H
#define MAIN_WINDOW__H


#include <ncursesw/curses.h>


#include "gui_common.h"
#include "../libfriclient/libfriclient.h"
#include "message_window.h"


void main_window_restore(FriclicliGui* gui, FriclientHandle* handle);


void main_window_show(FriclicliGui* gui, FriclientHandle* handle);


void main_window_show_posts(FriclicliGui* gui, FriclientHandle* handle);


#endif
