/*
    friclicli
    Copyright (C) 2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "friclicli_user_config.h"




FriclicliConfig* friclicli_config_read(const char* filename)
{
    FILE* config_file = fopen(filename, "r");
    if (config_file == NULL) {
        return NULL;
    }

    char* line = malloc(1025);
    if (line == NULL) {
        fclose(config_file);
        return NULL;
    }
    char* key = NULL;
    char* value = NULL;

    FriclicliConfig* config = malloc(sizeof(FriclicliConfig));
    if (config == NULL) {
        fclose(config_file);
        free(line);
        return NULL;
    }
    memset(config, 0, sizeof(FriclicliConfig));
    memset(line, 0, 1025);

    //read each line until end of file or until 1025 bytes are read:
    //TODO: avoid that 1025 byte limit.
    while (!feof(config_file)) {
        fgets(line, 1024, config_file);
        if (line[0] == '#') {
            //A comment line. Skip it!
            continue;
        }
        size_t i = 0;
        //States:
        //0 = before key
        //1 = key read
        //2 = after key
        //3 = before value
        //4 = value read
        //5 = after value (end of line)
        uint8_t parsing_state = 0;
        size_t key_start_pos = 0;
        size_t key_length = 0;
        size_t value_start_pos = 0;
        size_t value_length = 0;
        do {
            if ((line[i] == '\n') || (line[i] == '\0')) {
                parsing_state = 5;
                value_length = i - value_start_pos;
            } else {
                switch (parsing_state) {
                case 0: {
                    if (line[i] != ' ') {
                        parsing_state = 1;
                        key_start_pos = i;
                    }
                    break;
                }
                case 1: {
                    if (line[i] == ' ') {
                        parsing_state = 2;
                        key_length = i - key_start_pos;
                    } else if (line[i] == '=') {
                        parsing_state = 3;
                        key_length = i - key_start_pos;
                    }
                    break;
                }
                case 2: {
                    if (line[i] == '=') {
                        parsing_state = 3;
                    }
                    break;
                }
                case 3: {
                    if (line[i] != ' ') {
                        parsing_state = 4;
                        value_start_pos = i;
                    }
                    break;
                }
                case 4: {
                    //Nothing here.
                    break;
                }
                default: {
                    //Nothing at the moment.
                }
                }
            }
            i++;
        } while(parsing_state != 5);

        if ((key_length > 0) && (value_length > 0)) {
            //Copy key and value:
            if (key != NULL) {
                key = realloc(key, key_length + 1);
            } else {
                key = malloc(key_length + 1);
            }
            if (key == NULL) {
                continue;
            }
            if (value != NULL) {
                value = realloc(value, value_length + 1);
            } else {
                value = malloc(value_length + 1);
            }
            if (value == NULL) {
                free(key);
                continue;
            }
            memset(key, 0, key_length + 1);
            memset(value, 0, value_length + 1);
            strncpy(key, line + key_start_pos, key_length);
            strncpy(value, line + value_start_pos, value_length);

            //Store the setting in the config structure:
            friclicli_config_set_value(
                config,
                key,
                value
                );
        }
    }

    free(line);
    if (key != NULL) {
        free(key);
    }
    if (value != NULL) {
        free(value);
    }
    fclose(config_file);
    return config;
}


void friclicli_config_write(FriclicliConfig* config)
{
    
}


void friclicli_config_free_keys(
    FriclicliConfig* config
    )
{
    if (config == NULL) {
        return;
    }
    if (config->keys == NULL) {
        return;
    }

    size_t i = 0;
    for (i = 0; i < config->entries; i++) {
        if (config->keys[i] != NULL) {
            free(config->keys[i]);
        }
    }
    free(config->keys);
}


void friclicli_config_free_values(
    FriclicliConfig* config
    )
{
    if (config == NULL) {
        return;
    }
    if (config->values == NULL) {
        return;
    }

    size_t i = 0;
    for (i = 0; i < config->entries; i++) {
        if (config->values[i] != NULL) {
            free(config->values[i]);
        }
    }
    free(config->values);
}


char* friclicli_config_get_value(
    FriclicliConfig* config,
    const char* name
    )
{
    
}

void friclicli_config_set_value(
    FriclicliConfig* config,
    const char* name,
    const char* value
    )
{
    if ((config == NULL) || (name == NULL) || (value == NULL)) {
        return;
    }

    //Check for special configuration parameters first:
    if (strcmp(name, "username") == 0) {
        size_t username_length = strlen(name);
        if (config->username != NULL) {
            config->username = realloc(config->username, username_length + 1);
        } else {
            config->username = malloc(username_length + 1);
        }
        memset(config->username, 0, username_length + 1);
        strncpy(config->username, name, username_length);
        return;
    }
    if (strcmp(name, "password") == 0) {
        size_t password_length = strlen(name);
        if (config->username != NULL) {
            config->password = realloc(config->password, password_length + 1);
        } else {
            config->password = malloc(password_length + 1);
        }
        memset(config->password, 0, password_length + 1);
        strncpy(config->password, name, password_length);
        return;
    }
    if (strcmp(name, "host") == 0) {
        size_t host_length = strlen(name);
        if (config->host != NULL) {
            config->host = realloc(config->host, host_length + 1);
        } else {
            config->host = malloc(host_length + 1);
        }
        memset(config->host, 0, host_length + 1);
        strncpy(config->host, name, host_length);
        return;
    }

    size_t new_index = 0;
    if (config->keys == NULL) {
        //Reserve space for one char* pointer:
        config->keys = malloc(sizeof(char*));
        if (config->keys == NULL) {
            //We cannot continue.
            return;
        }
    } else {
        //Add one entry:
        config->keys = realloc(config->keys, sizeof(char*) * (config->entries + 1));
        if (config->keys == NULL) {
            //We cannot continue;
            if (config->values != NULL) {
                friclicli_config_free_values(config);
            }
            return;
        }
    }
    if (config->values == NULL) {
        config->values = malloc(sizeof(char*));
        if (config->values == NULL) {
            //We cannot continue.
            return;
        }
    } else {
        //Add one entry:
        config->values = realloc(config->values, sizeof(char*) * (config->entries + 1));
        if (config->values == NULL) {
            //We cannot continue.
            if (config->keys != NULL) {
                friclicli_config_free_keys(config);
            }
        }
    }
    new_index = config->entries;

    config->keys[new_index] = malloc(65);
    if (config->keys[new_index] == NULL) {
        config->keys = realloc(config->keys, sizeof(char*) * (config->entries));
        config->values = realloc(config->values, sizeof(char*) * (config->entries));
        return;
    }
    config->values[new_index] = malloc(961);
    if (config->values[new_index] == NULL) {
        config->values = realloc(config->values, sizeof(char*) * (config->entries));
        free(config->keys[new_index]);
        config->keys = realloc(config->keys, sizeof(char*) * (config->entries));
    }
    memset(config->keys[new_index], 0, 65);
    memset(config->values[new_index], 0, 961);

    strncpy(config->keys[new_index], name, 64);
    strncpy(config->values[new_index], value, 960);
    
}
